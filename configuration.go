/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk
 * https://malk.io
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package malk

import (
	"encoding/json"
	"encoding/xml"
	"gitlab.com/chriseaton/malk-storage"
	"io"
)

// The malk configuration can be customized as you wish by creating your own struct definition for other global
// settings you wish to include throughout a given malk server instance. All that is required is that it provides
// the defined function calls in this interface.
type Configuration interface {

	// Retrieves the configuration containing server information.
	ServerConfig() *ServerConfiguration

	// Retrieves the configuration containing dashboard information.
	DashboardConfig() *DashboardConfiguration

	// Retrieves teh configuration containing driver configuration.
	StorageConfig() *storage.DriverConfiguration

	// This function is called when malk is Configured. It gives custom Configuration interface implementations the
	// ability to apply custom settings as needed. This is called at the end of the Configure function.
	Configure(srv *Server) error
}

type ServerConfiguration struct {

	// The interface (ip) address to bind to. If left blank, malk will bind to all interfaces.
	Host string `xml:"Host" json:"host"`

	// The port to serve from. If left as 0, it will be configured at runtime to port 8080.
	Port int `xml:"Port" json:"port"`

	// Path to a site-wide static file directory. All non-hidden files in this directory will be accessible to
	// any GET-method requestor. When blank, malk uses "static" as the default directory.
	StaticDirectory string `xml:"StaticDirectory" json:"static-dir"`

	// Path to the template directory containing the "dashboard", "errors", and "themes" directories.
	// When blank, malk uses "themes" as the default directory.
	ThemeDirectory string `xml:"ThemeDirectory" json:"theme-dir"`

	// Specifies the storage engine to use, can be any of the following:
	// "file"      = File storage driver (JSON or XML), see: https://gitlab.com/chriseaton/malk-file-storage
	// "datastore" = Google (Cloud) Datastore driver, see: https://gitlab.com/chriseaton/malk-datastore-storage
	// "sqlite"    = SQLite3 storage driver, see: https://gitlab.com/chriseaton/malk-sqlite-storage
	// "custom"    = Indicates a custom storage driver is used. Custom storage drivers are supported through the
	//               malk.Server.SetStorageDriver function and malk.StorageDriver interface.
	//
	// When blank, the "file" value is used as the default.
	StorageDriver string `xml:"StorageDriver,omitempty" json:"storage-driver,omitempty"`
}

type DashboardConfiguration struct {
	PathPrefix string `xml:"PathPrefix" json:"path"`

	SSLRequired bool `xml:"SSLRequired" json:"ssl"`
}

// BasicConfiguration is a pre-constructed configuration struct that can be used to get up and running quickly
// with malk. Assuming you are fine with the default values, you don't need to set any field values.
type BasicConfiguration struct {
	Server ServerConfiguration `xml:"Server" json:"server"`

	Dashboard DashboardConfiguration `xml:"Dashboard,omitempty" json:"dashboard,omitempty"`

	Storage storage.DriverConfiguration `xml:"Storage,omitempty" json:"storage,omitempty"`
}

// Returns the server configuration.
func (s *BasicConfiguration) ServerConfig() *ServerConfiguration {
	return &s.Server
}

// Returns the dashboard configuration.
func (s *BasicConfiguration) DashboardConfig() *DashboardConfiguration {
	return &s.Dashboard
}

// Returns the storage configuration.
func (s *BasicConfiguration) StorageConfig() *storage.DriverConfiguration {
	return &s.Storage
}

// Nothing special is done with this Configuration struct
func (s *BasicConfiguration) Configure(srv *Server) error {
	return nil
}

// Saves the configuration in XML format to the specified writing stream.
func (s *BasicConfiguration) SaveXML(w io.Writer) error {
	b, err := xml.Marshal(s)
	if err != nil {
		return err
	}
	_, err = w.Write(b)
	return err
}

// Saves the configuration in JSON format to the specified writing stream.
func (s *BasicConfiguration) SaveJSON(w io.Writer) error {
	b, err := json.Marshal(s)
	if err != nil {
		return err
	}
	_, err = w.Write(b)
	return err
}

// Reads the configuration from an XML reader stream.
func (s *BasicConfiguration) LoadXML(r io.Reader) error {
	err := xml.NewDecoder(r).Decode(s)
	if err != nil {
		return err
	}
	return nil
}

// Reads the configuration from an JSON reader stream.
func (s *BasicConfiguration) LoadJSON(r io.Reader) error {
	err := json.NewDecoder(r).Decode(s)
	if err != nil {
		return err
	}
	return nil
}
