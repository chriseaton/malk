/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk
 * https://malk.io
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package malk

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

type Dashboard struct {
	Server *Server
}

func (d *Dashboard) RegisterRoutes(root *mux.Router, dc *DashboardConfiguration) {
	var route *mux.Route
	path := dc.PathPrefix
	if path == "" {
		path = "/dashboard/"
	}
	if dc.SSLRequired {
		route = root.Schemes("https")
	} else {
		route = root.Schemes("http", "https")
	}
	sr := route.PathPrefix(path).Subrouter()
	//add routes
	sr.HandleFunc("pages/{slug:[A-z0-9-]+", d.HandlePages).Methods("GET")
	sr.HandleFunc("pages/{slug:[A-z0-9-]+", d.HandlePages).Methods("POST")
}

func (d *Dashboard) HandlePages(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug := vars["slug"]
	if slug != "" {
		//show edit page
	} else {
		//show page listing
	}
}

func (d *Dashboard) HandlePageEdit(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug := vars["slug"]
	if slug != "" {
		//save
	} else {
		d.Server.HandleError(w, 404, fmt.Errorf("Cannot save page, the slug was not specified in the request."))
	}
}
