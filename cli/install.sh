#!/bin/bash

echo "Malk: The CMS with vitamin R."
echo "https://gitlab.com/chriseaton/malk"
echo

# Change working directory to script location.
cd ${0%/*}

# Start build
echo "Building..."
if ! [ -x "$(command -v go)" ]; then
	echo "The \"go\" command is missing or unavailable (is go installed?)."
	exit 3
fi
go build -o malk

# Install to PATH
read -p "Would you like to add the malk tool to your PATH so you can run it from anywhere? (y/n): " -n 1 -r expath
echo
if [[ $expath =~ ^[Yy]$ ]]
then
	echo "export PATH=\$PATH:$(pwd)" | tee -a ~/.bashrc
	echo "Added PATH export to \"~/.bashrc\"."
	# Add to path in current terminal
	export PATH=\$PATH:$(pwd)
fi

# Done!
echo "All done!"