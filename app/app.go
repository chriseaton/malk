package myapp

import (
	"https://github.com/chriseaton/malk"
)

func main() {
	//simple way to get started with the default settings.
	//see for other methods that provide tighter control see:
	//{some url}
	//
	//using Google AppEngine? See here:
	malk.ListenAndServe(nil)
}