/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk
 * https://malk.io
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package malk

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/chriseaton/malk-storage"
	"gitlab.com/chriseaton/malk-storage-file"
	"net/http"
	"strings"
	// "github.com/gorilla/schema"
	// "github.com/gorilla/securecookie"
	// "github.com/gorilla/sessions"
	// "github.com/gorilla/feeds"
)

var validMinPortNumber = 1
var validMaxPortNumber = 65535

// The malk server represents a single instance of the Malk CMS. You can setup multiple Malk instances within your
// application if you wish (although they cannot share the same port).
type Server struct {

	// The active configuration struct.
	Configuration Configuration

	// The active (root) router used by the running malk instance. If the server is stopped, this field will be nil.
	Router *mux.Router

	// The register holding model entries, used by malk drivers.
	registry *storage.EntityModelRegistry

	// The current storage driver used by malk. If this is not configured, the default file-storage driver will be
	// used. See: https://gitlab.com/chriseaton/malk-file-storage
	storage storage.Driver

	// Function called when an error is encountered and handled by malk. You can utilize this function to implement
	// your own custom error logging.
	ErrorReport func(statusCode int, err error)
}

// Creates a Malk Server.
func CreateServer() *Server {
	s := &Server{}
	return s
}

// Creates a new malk server and begins serving. If a configuration object is not given (nil) a default configuration
// is created.
func ListenAndServe(c Configuration) (*Server, error) {
	s := &Server{}
	if c == nil {
		c = &BasicConfiguration{}
	}
	err := s.Configure(c)
	if err != nil {
		return s, err
	}
	return s, s.ListenAndServe()
}

// Sets up the server with the given configuration. Initializes the router and default routes.
func (srv *Server) Configure(c Configuration) error {
	var err error
	if c == nil && srv.Configuration == nil {
		return fmt.Errorf("A configuration value must be specified.")
	}
	if err = srv.validateConfiguration(c); err != nil {
		return err
	}
	if c != nil {
		srv.Configuration = c
	}
	sc := srv.Configuration.ServerConfig()
	dashc := srv.Configuration.DashboardConfig()
	storec := srv.Configuration.StorageConfig()
	d := &Dashboard{
		Server: srv,
	}
	//apply defaults
	if sc.Port == 0 {
		sc.Port = 8080
	}
	if sc.ThemeDirectory == "" {
		sc.ThemeDirectory = "themes"
	}
	if sc.StaticDirectory == "" {
		sc.StaticDirectory = "static"
	}
	//initialize registry
	srv.registry = &storage.EntityModelRegistry{}
	//initialize the storage driver
	sdrv := strings.ToLower(sc.StorageDriver)
	if sdrv == "file" {
		srv.storage, err = filestorage.Create("", "")
	} else if sdrv == "datastore" {

	} else if sdrv == "sqlite" {

	} else if sdrv == "custom" {
		if srv.storage == nil {
			return fmt.Errorf("A custom storage driver was indicated in the configuration, but no driver has been set.")
		}
	} else {
		return fmt.Errorf("Invalid storage configuration value.")
	}
	srv.storage.Configure(storec, srv.registry)
	//build routings
	srv.Router = mux.NewRouter()
	d.RegisterRoutes(srv.Router, dashc)
	//call configuration's custom Configure function
	if err = c.Configure(srv); err != nil {
		return err
	}
	return nil
}

// Updates the server's storage driver to a custom one
func (srv *Server) SetStorageDriver(d storage.Driver) error {
	sc := srv.Configuration.ServerConfig()
	if sc != nil {
		sc.StorageDriver = "custom"
		srv.storage = d
		return nil
	}
	return fmt.Errorf("Server configuration could not be updated because a nil value was returned from the Configuration interface ServerConfig() function.")
}

// Starts the malk server and listens for connections. If the server config
func (srv *Server) ListenAndServe() error {
	if srv.Configuration == nil {
		return fmt.Errorf("Server's Configuration field cannot be nil.")
	} else if srv.Router == nil {
		return fmt.Errorf("Server is missing Router. You should call Configure before calling ListenAndServe.")
	}
	sc := srv.Configuration.ServerConfig()
	return http.ListenAndServe(fmt.Sprintf("%s:%d", sc.Host, sc.Port), srv.Router)
}

// Validates the values provided in the given Configuration.
func (srv *Server) validateConfiguration(c Configuration) error {
	//check server configuration
	sc := c.ServerConfig()
	if sc.Port != 0 && (sc.Port < validMinPortNumber || sc.Port > validMaxPortNumber) {
		return fmt.Errorf("Invalid port number. The port number should be between %d and %d.", validMinPortNumber, validMaxPortNumber)
	}
	//check dashboard configuration
	return nil
}

func (srv *Server) HandleError(w http.ResponseWriter, statusCode int, err error) {
	//look for error template

	//report error
	if srv.ErrorReport != nil {
		srv.ErrorReport(statusCode, err)
	}
}
