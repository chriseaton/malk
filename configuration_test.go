/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk
 * https://malk.io
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package malk

import (
	// "bytes"
	// "strings"
	"testing"
)

func TestConfigure(t *testing.T) {
	err := new(Server).validateConfiguration(&BasicConfiguration{})
	if err != nil {
		t.Errorf("Error configuring server with default SimpleConfiguration. Error: %s", err)
	}
	err = new(Server).validateConfiguration(&BasicConfiguration{
		Server: ServerConfiguration{
			Host: "some.example.com",
			Port: 9999,
		},
	})
	if err != nil {
		t.Errorf("Error configuring server with SimpleConfiguration. Error: %s", err)
	}
}

// func TestSaveXML(t *testing.T) {
// 	buf := new(bytes.Buffer)
// 	c := &BasicConfiguration{
// 		Server: ServerConfiguration{
// 			Host: "some.example.com",
// 			Port: 9999,
// 		},
// 	}
// 	err := c.SaveXML(buf)
// 	if err != nil {
// 		t.Errorf("Error writing XML to buffer. Error: %s", err)
// 	}
// }

// func TestLoadXML(t *testing.T) {
// 	c := &BasicConfiguration{}
// 	err := c.LoadXML(strings.NewReader(validXML))
// 	if err != nil {
// 		t.Errorf("Error writing XML to buffer. Error: %s", err)
// 	} else if c.Server.Host != "some.example.com" {
// 		t.Error("Unexpected \"Host\" value in XML.")
// 	} else if c.Server.Port != 9999 {
// 		t.Error("Unexpected \"Port\" value in XML.")
// 	} else if c.Server.CloudEnvironment != "" {
// 		t.Error("Unexpected \"Cloud\" value in XML.")
// 	}
// }

// func TestSaveJSON(t *testing.T) {
// 	buf := new(bytes.Buffer)
// 	c := &BasicConfiguration{
// 		Server: ServerConfiguration{
// 			Host: "some.example.com",
// 			Port: 9999,
// 		},
// 	}
// 	err := c.SaveJSON(buf)
// 	if err != nil {
// 		t.Errorf("Error writing JSON to buffer. Error: %s", err)
// 	}
// }

// func TestLoadJSON(t *testing.T) {
// 	c := &BasicConfiguration{}
// 	err := c.LoadJSON(strings.NewReader(validJSON))
// 	if err != nil {
// 		t.Errorf("Error writing JSON to buffer. Error: %s", err)
// 	} else if c.Server.Host != "some.example.com" {
// 		t.Error("Unexpected \"Host\" value in JSON.")
// 	} else if c.Server.Port != 9999 {
// 		t.Error("Unexpected \"Port\" value in JSON.")
// 	} else if c.Server.CloudEnvironment != "" {
// 		t.Error("Unexpected \"Cloud\" value in JSON.")
// 	}
// }
