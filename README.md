![Alt text](https://gitlab.com/chriseaton/malk/uploads/ce7be41bcaee118a311aa5991007aa5a/malk-128x128.png "Optional title")
# [malk](https://gitlab.com/chriseaton/malk)
Malk is a *[content management system](https://en.wikipedia.org/wiki/Content_management_system)* (CMS) written in
[Go](https://golang.org/).

## NOT YET USABLE!
Malk just started development March 2016. It is *not* ready for testing, production, or really any type of use.

### Features

### In the Works
 - A command-line tool.
 - An administrative dashboard interface.
 - Page creation, editing, and general management.
 - Theming.
 - User management
   - Custom authentication methods.
   - A built-in authorization scheme (roles).
 - Categorical lists.
 - Media libraries.
 - Data modelling.
 - Data storage medium agnostic.
   - Local file (JSON)
   - SQLite
   - MySQL
   - Google Cloud Datastore
 - Dedicated website ([malk.io](http://malk.io))
   - Theme browser.
   - Add-on listing.

# F.A.Q.

### How's it put together?
Malk is written in [Go](http://golang.org), leveraging the standard Go libraries and the popular
[Gorilla web toolkit](http://www.gorillatoolkit.org/) as a solid foundation for core operations.

Data storage is diversified into seperate projects, and is designed to be expandable and customizable. By default,
Malk includes the [file storage driver](https://gitlab.com/chriseaton/malk-file-storage), which reads and writes all
CMS data to JSON or XML files.

##### Why Gorilla?
Gorilla was chosen due to it's long and well-established position within the go community. Using this well documented
tookit makes Malk much more extensible and recognizable to new users, and allows contributors to better focus on
Malk's features.

### Can I use it for free or commercial purposes?
Yes! Malk is released under the
[Universal Permissive License](https://tldrlegal.com/license/universal-permissive-license-1.0-(upl-1.0))
. Use it however you like, re-brand it, change it,
delete things, whatever you want (as long as the license is followed).

### Can I contribute to the project?
Yes! This project is open-source, we want you to provide feedback, resolve bugs, build new features or add-ons, or just
help out any way you can. See our
[document for contributing](https://gitlab.com/chriseaton/malk/blob/master/CONTRIBUTING.md).