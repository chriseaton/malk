![malk - now with vitamin R!](https://gitlab.com/chriseaton/malk/uploads/ce7be41bcaee118a311aa5991007aa5a/malk-128x128.png "malk - now with vitamin R!")
# [malk](https://gitlab.com/chriseaton/malk)

## Contributing
Thinking about helping out? First, thank you, we want to make Malk a great CMS that's open and free to anybody who
wants to use it.

### So how can you help?
 - Consider looking through the
   [open issues](https://gitlab.com/chriseaton/malk/issues?scope=all&sort=id_desc&state=opened&utf8=✓&assignee_id=0)
   that haven't been assigned and see if you can resolve them.
 - Help create and maintain our [project wiki](https://gitlab.com/chriseaton/malk/wikis/home).
 - Create new and interesting features for the Malk project (see code contributions below).
 - Build your own project that leverages Malk.
 - Spread the word, encourage people to try Malk.
   - Use a badge on your website:

     ![16x16](media/badges/malk-badge-16x16.png)
     ![32x16](media/badges/malk-badge-32x16.png)
     ![32x32](media/badges/malk-badge-32x32.png)
     ![50x25](media/badges/malk-badge-50x25.png)
     ![64x64](media/badges/malk-badge-64x64.png)
     ![120x50](media/badges/malk-badge-120x50.png)
     ![128x128](media/badges/malk-badge-128x128.png)
     ![140x140](media/badges/malk-badge-140x140.png)
     ![185x50](media/badges/malk-badge-185x50.png)

### Guidelines
We are open to contributions, but that doesn't mean we accept every revision or piece of content. Your contribution
must relevant to the project, follow the guidelines in this document, and generally follow the same look, feel, and
architecture where relevant. Text, comments, and media should be clear and
professional (well, mostly professional).

#### Git
Commits without relevant messages will not be accepted.

#### Go Code
 1. Must be formatted with:
    ````go fmt````
 2. Must be successfully vetted with:

    ````go tool vet -composites=false -unusedresult=false {filename}````
 3. Never use ````panic````, return an ````error```` instead.
 4. Should compile without error (obvious right?).
 5. Should never import packages from sub-directories.
 6. Must include meaningful comments over every function, struct, field definition, and interface.
 7. Be backwards compatible with go v1.4.
 8. New *.go files must include the standard license header:
    ````
    /*****************************************************************************************
     * Copyright (c) 2016 Christopher Eaton
     * https://github.com/chriseaton/malk
     * https://malk.io
     * This source code is subject to the terms of the the Universal Permissive License v1.0.
     ****************************************************************************************/
    ````
 9. Should, within reason, keep go files aligned with a 120 character margin (not 80, it's not the 1920s anymore, we
    all have decently sized monitors...)

#### Markdown
 1. Must be compatible with GitLabs's markdown engine.
 2. Text must be wrapped at a 120 character margin. The only exception should be long URLs.
 3. No shortened links.